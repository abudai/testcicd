import sys
from pathlib import Path


def count_characters(file_path: Path):
    try:
        with open(file_path, 'r') as current_file:
            text = current_file.read()
            char_count = len(text)  # Flake8 error: Remove extra space
            return char_count
    except FileNotFoundError:
        return -1


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print('Usage: python character_count.py <file_path>')
        sys.exit(1)

    file_path = sys.argv[1]
    result = count_characters(file_path)
    if result == -1:
        print('File not found.')
    else:
        print(f'Character count: {result}')
