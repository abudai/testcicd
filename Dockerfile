FROM python:3.11-slim
WORKDIR /code

COPY src/ .

CMD ["python", "./character_count.py"]